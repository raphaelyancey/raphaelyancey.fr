---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

<div style="display: flex; margin-bottom: 30px;" id="introduction">
<div>
    {% picture /assets/img/me.jpg --alt Raphaël Yancey --img style="width: 200px; border-radius: 2px;" %}
</div>
<div style="margin-left: 20px;">
<p>Welcome to my personal pages. Here you'll find posts and <a href="{{ "/projects" | relative_url }}">projects</a> involving <b>programming</b>, <b>electronics</b>, <b>music</b> and other topics.</p>
</div>
</div>
